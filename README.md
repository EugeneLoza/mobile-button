# Mobile Button

An extension of TCastleButton accepting Long-press event

# License

License is the same as Castle Game Engine, unless explicitly specified, see https://castle-engine.io/license.php
